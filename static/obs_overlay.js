function refreshCurrentEvent() {
    $.ajax("/api/now").done(function(response) {
        if(response && response.current_match) {
            $(".team-name.blue").html(response.current_match.blue_team);
            $(".team-name.gold").html(response.current_match.gold_team);
            $(".team-score.blue").html(response.current_match.blue_score);
            $(".team-score.gold").html(response.current_match.gold_score);
            return;
        }

        if(response && response.active_matches) {
            for(match of response.active_matches) {
                if(match.cabinet_name.toLowerCase() == cabinetName.toLowerCase()) {
                    $(".team-name.blue").html(match.blue_team);
                    $(".team-name.gold").html(match.gold_team);
                    $(".team-score.blue").html(match.blue_score);
                    $(".team-score.gold").html(match.gold_score);
                    return;
                }
            }
        }

        $(".team-name.blue").html("Blue Team");
        $(".team-name.gold").html("Gold Team");
        $(".team-score.blue").html("");
        $(".team-score.gold").html("");
    });
}

$(document).ready(function() {
    refreshCurrentEvent();
    setInterval(refreshCurrentEvent, 10000);
});
