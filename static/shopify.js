var scriptURL = 'https://sdks.shopifycdn.com/buy-button/latest/buy-button-storefront.min.js';
if (window.ShopifyBuy) {
  if (window.ShopifyBuy.UI) {
    ShopifyBuyInit();
  } else {
    loadScript();
  }
} else {
  loadScript();
}

function loadScript() {
  var script = document.createElement('script');
  script.async = true;
  script.src = scriptURL;
  (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(script);
  script.onload = ShopifyBuyInit;
}

function ShopifyBuyInit() {
  var client = ShopifyBuy.buildClient({
    domain: 'custom-3d-stuff.myshopify.com',
    storefrontAccessToken: '106fa24de9fe45e98416248541ebe2d8',
  });

  ShopifyBuy.UI.onReady(client).then(function (ui) {
    ui.createComponent('product', {
      id: [2341816107121],
      node: document.getElementById('product-component-d7f27efe4f9'),
      moneyFormat: '%24%7B%7Bamount%7D%7D',
      options: {
        "product": {
          "variantId": "all",
          "contents": {
            "imgWithCarousel": false,
            "variantTitle": false,
            "description": false,
            "buttonWithQuantity": false,
            "quantity": false
          },
          "styles": {
            "product": {
              "@media (min-width: 601px)": {
                "max-width": "calc(25% - 20px)",
                "margin-left": "20px",
                "margin-bottom": "50px"
              }
            }
          }
        },
        "cart": {
          "contents": {
            "button": true
          },
          "styles": {
            "footer": {
              "background-color": "#ffffff"
            }
          }
        },
        "modalProduct": {
          "contents": {
            "img": false,
            "imgWithCarousel": true,
            "variantTitle": false,
            "buttonWithQuantity": true,
            "button": false,
            "quantity": false
          },
          "styles": {
            "product": {
              "@media (min-width: 601px)": {
                "max-width": "100%",
                "margin-left": "0px",
                "margin-bottom": "0px"
              }
            }
          }
        },
        "productSet": {
          "styles": {
            "products": {
              "@media (min-width: 601px)": {
                "margin-left": "-20px"
              }
            }
          }
        }
      }
    });
  });
}
