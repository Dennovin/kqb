jQuery.fn.reverse = [].reverse;

function setupTwitchStream() {
    var streamDiv = $("#twitch-stream");
    if(streamDiv.length == 0) {
        return;
    }

    $.ajax({
        type: "GET",
        url: "https://api.twitch.tv/kraken/streams.json?channel=baltimorekillerqueen",
        headers: {
            "Content-Type": "application/json",
            "Client-ID": streamDiv.attr("clientid")
        }
    }).done(function(response) {
        if(response._total > 0) {
            var options = {
                height: 360,
                width: 640,
                channel: "baltimorekillerqueen",
            };

            var player = new Twitch.Player("twitch-stream", options);
        }
    });
}

function refreshCurrentEvent() {
    $.ajax("/api/now").done(function(response) {
        $(".current-match .blue.team").html(response.current_match.blue_team);
        $(".current-match .blue.score").html(response.current_match.blue_score);
        $(".current-match .gold.team").html(response.current_match.gold_team);
        $(".current-match .gold.score").html(response.current_match.gold_score);

        var standings = $(".standings.event-standings");
        $.each(response.standings, function(i, team) {
            var rowgrp = standings.find(".row-group[teamid=" + team.team_id + "]");
            var cells = rowgrp.find(".cell");

            cells.eq(0).html(team.team_name);
            cells.eq(1).html(team.points);
            cells.eq(2).html(team.rounds);
            cells.eq(3).html(team.matches);
        });

        $(".event-results .row.data-row").remove();
        var remaining = $(".event-results.remaining");
        var completed = $(".event-results.completed");
        $.each(response.matches, function(i, match) {
            var row = $("<div />").addClass("row data-row");

            var blueResult = $("<div />").addClass("blue result");
            blueResult.append(
                $("<div />").addClass("blue team").html(match.blue_team),
                $("<div />").addClass("blue score")
            );

            var goldResult = $("<div />").addClass("gold result");
            goldResult.append(
                $("<div />").addClass("gold team").html(match.gold_team),
                $("<div />").addClass("gold score").html(match.gold_score)
            );

            if(match.is_complete) {
                blueResult.find(".score").html(match.blue_score);
                goldResult.find(".score").html(match.blue_score);

                if(match.blue_score > match.gold_score) {
                    blueResult.addClass("winner");
                } else {
                    goldResult.addClass("winner");
                }

                row.appendTo(completed);
            } else {
                row.appendTo(remaining);
            }
        });
    });
}

function sortValue(cell) {
    if(cell[0].hasAttribute("sort-value"))
        return cell.attr("sort-value");
    else
        return cell.text();
}

function sortRows(e) {
    var row = $(this).closest(".header-row");
    var idx = row.children(".cell").index($(this));

    var table = $(this).closest(".sortable")
    var rows = table.children(".data-row");

    if($(this).hasClass("numeric")) {
        rows.sort(function(a, b) { return parseFloat(sortValue($(a).children(".cell").eq(idx))) - parseFloat(sortValue($(b).children(".cell").eq(idx))); });
    } else {
        rows.sort(function(a, b) { return sortValue($(a).children(".cell").eq(idx)).localeCompare(sortValue($(b).children(".cell").eq(idx))); });
    }

    isAscending = $(this).hasClass("sorted-ascending");
    row.children(".cell").removeClass("sorted-ascending").removeClass("sorted-descending");

    if(isAscending) {
        rows = rows.reverse();
        $(this).addClass("sorted-descending");
    } else {
        $(this).addClass("sorted-ascending");
    }

    rows.remove().appendTo(table);
}

function loadTournamentStandings() {
    var standings = $(".tournament-group-standings");
    if(standings.length == 0)
        return;

    var tournamentID = $("input[name=tournament_id]").val();
    $.ajax(`/api/tournament/${tournamentID}/standings`).done(function(response) {
        for(var bracket of response.brackets) {
            var bracketDiv = $("<div />").addClass("bracket");
            $("<div />").addClass("name").html(bracket.name).appendTo(bracketDiv);

            for(var team of bracket.standings) {
                var row = $("<div />").addClass("row");
                $("<div />").addClass("team-name").html(team.name).appendTo(row);
                $("<div />").addClass("score").html(team.score.toFixed(1)).appendTo(row);
                row.appendTo(bracketDiv);
            }

            bracketDiv.appendTo(standings);
        }

        standings.removeClass("loading");
    });
}

$(document).ready(function() {
    setupTwitchStream();
    loadTournamentStandings();

    if($(".event-results .current-match").length > 0) {
        setInterval(refreshCurrentEvent, 30000);
    }

    $("body")
        .on("click", ".sortable .header-row .cell", sortRows)
    ;
});
