Chart.defaults.SnailLineChart = Chart.helpers.clone(Chart.defaults.line);
Chart.controllers.SnailLineChart = Chart.controllers.line.extend({
    update: function () {
        // get the min and max values
        var min = this.chart.data.datasets[0].data.reduce((min, p) => p.y < min ? p.y : min, this.chart.data.datasets[0].data[0].y);
        var max = this.chart.data.datasets[0].data.reduce((max, p) => p.y > max ? p.y : max, this.chart.data.datasets[0].data[0].y);

        if(min == 0 && max == 0) {
            this.chart.data.datasets[0].backgroundColor = "#444";
            return Chart.controllers.line.prototype.update.apply(this, arguments);
        }

        var yScale = this.getScaleForId(this.getDataset().yAxisID);

        // figure out the pixels for these and the value 0
        var top = yScale.getPixelForValue(max);
        var zero = yScale.getPixelForValue(0);
        var bottom = yScale.getPixelForValue(min);

        // build a gradient that switches color at the 0 point
        var ctx = this.chart.chart.ctx;
        var gradient = ctx.createLinearGradient(0, top, 0, bottom);
        var ratio = Math.min((zero - top) / (bottom - top), 1);
        gradient.addColorStop(0, "#1acbb0");
        gradient.addColorStop(ratio, "#1acbb0");
        gradient.addColorStop(ratio, "#ffa70e");
        gradient.addColorStop(1, "#ffa70e");
        this.chart.data.datasets[0].backgroundColor = gradient;

        return Chart.controllers.line.prototype.update.apply(this, arguments);
    }
});

function formatTime(totalSeconds) {
    var minutes = Math.floor(totalSeconds / 60);
    var seconds = String(Math.floor(totalSeconds % 60)).padStart(2, "0");
    return `${minutes}:${seconds}`;
}

$(document).ready(function() {
    var game_id = $("input[name=game_id]").val();
    $.ajax("/api/game/" + game_id).done(function(response) {
        var ctx = $("#snail-chart").get(0).getContext("2d");
        var snailChart = new Chart(ctx, {
            type: "SnailLineChart",
            data: {
                datasets: [{
                    label: "Snail Progress",
                    borderColor: "#555",
                    pointRadius: 0,
                    data: response.snail_data,
                    fill: "origin",
                    borderWidth: 1,
                    lineTension: 0,
                    yAxisID : "y-axis-0",
                }]
            },
            options: {
                title: {display: false},
                legend: {display: false},
                animation: {duration: 0},
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                        display: true,
                        type: "linear",
                        scaleLabel: {
                            display: false,
                            labelString: "Seconds",
                        },
                        ticks: {
                            callback: (value) => formatTime(value),
                            min: 0,
                            max: response.length_sec,
                        },
                    }],
                    yAxes: [{
                        display: false,
                        type: "linear",
                        scaleLabel: {
                            display: false,
                            labelString: "Pixels",
                        },
                        ticks: {
                            min: -900,
                            max: 900,
                        },
                    }],
                },
            },
        });

        ctx = $("#berry-chart").get(0).getContext("2d");
        var berryChart = new Chart(ctx, {
            type: "line",
            data: {
                datasets: [
                    {
                        label: "Blue Berries",
                        borderColor: "#128f7c",
                        backgroundColor: "#1acbb0",
                        pointRadius: 0,
                        data: response.berry_data.blue,
                        borderWidth: 1,
                        lineTension: 0,
                    },
                    {
                        label: "Gold Berries",
                        borderColor: "#c97f00",
                        backgroundColor: "#ffa70e",
                        pointRadius: 0,
                        data: response.berry_data.gold.map((v) => { return {x: v.x, y: -v.y}; }),
                        borderWidth: 1,
                        lineTension: 0,
                    },
                ],
            },
            options: {
                title: {display: false},
                legend: {display: false},
                animation: {duration: 0},
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                        display: true,
                        type: "linear",
                        scaleLabel: {
                            display: false,
                            labelString: "Seconds",
                        },
                        ticks: {
                            callback: (value) => formatTime(value),
                            min: 0,
                            max: response.length_sec,
                        },
                    }],
                    yAxes: [{
                        display: false,
                        type: "linear",
                        scaleLabel: {
                            display: false,
                            labelString: "Berries",
                        },
                        ticks: {
                            min: -12,
                            max: 12,
                        },
                    }],
                },
            },
        });

        ctx = $("#warrior-chart").get(0).getContext("2d");
        var warriorChart = new Chart(ctx, {
            type: "line",
            data: {
                datasets: [
                    {
                        label: "Blue Warriors Up",
                        borderColor: "#128f7c",
                        backgroundColor: "#1acbb0",
                        pointRadius: 0,
                        data: response.warrior_data.blue,
                        borderWidth: 1,
                        lineTension: 0,
                    },
                    {
                        label: "Gold Warriors Up",
                        borderColor: "#c97f00",
                        backgroundColor: "#ffa70e",
                        pointRadius: 0,
                        data: response.warrior_data.gold.map((v) => { return {x: v.x, y: -v.y}; }),
                        borderWidth: 1,
                        lineTension: 0,
                    },
                ],
            },
            options: {
                title: {display: false},
                legend: {display: false},
                animation: {duration: 0},
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                        display: true,
                        type: "linear",
                        scaleLabel: {
                            display: false,
                            labelString: "Seconds",
                        },
                        ticks: {
                            callback: (value) => formatTime(value),
                            min: 0,
                            max: response.length_sec,
                        },
                    }],
                    yAxes: [{
                        display: false,
                        type: "linear",
                        scaleLabel: {
                            display: false,
                            labelString: "Warriors Up",
                        },
                        ticks: {
                            min: -4,
                            max: 4,
                        },
                    }],
                },
            },
        });
    });
});
