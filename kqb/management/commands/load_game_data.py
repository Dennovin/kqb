import pytz
from datetime import datetime
from django.core.management.base import BaseCommand
from kqb.models import Event, Game

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("first_game_id")
        parser.add_argument("event_id")

    def handle(self, *args, **kwargs):
        event = Event.objects.get(id=kwargs["event_id"])

        game_id = int(kwargs["first_game_id"])
        for match in event.match_set.order_by("id").all():
            for i in range(game_id, game_id + match.blue_score + match.gold_score):
                game = Game.get_game_data(i, match.id)
                game.save()

            game_id += match.blue_score + match.gold_score
