from django.core.management.base import BaseCommand
from kqb.models import AdminUser

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("emails", nargs="+")

    def handle(self, *args, **kwargs):
        for email in kwargs["emails"]:
            try:
                user = AdminUser.objects.filter(email=email).get()
                user.authorized = True
                user.save()
                self.stdout.write(self.style.SUCCESS("Authorized user {} for admin access.".format(email)))

            except AdminUser.DoesNotExist:
                self.stdout.write(self.style.NOTICE("User {} not found.".format(email)))
