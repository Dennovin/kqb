# Generated by Django 2.2.3 on 2019-09-02 21:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kqb', '0016_auto_20190902_0045'),
    ]

    operations = [
        migrations.AddField(
            model_name='tournamentbracket',
            name='rounds_per_match',
            field=models.IntegerField(default=0),
        ),
    ]
