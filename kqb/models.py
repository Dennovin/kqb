import hashlib
import itertools
import math
import os
import pytz
import random
import requests
import trueskill
from datetime import datetime, timedelta
from decimal import Decimal
from django.db import models, transaction
from django.conf import settings
from django.contrib.postgres.fields import ArrayField


trueskill_env = trueskill.TrueSkill(draw_probability=0.0)

TEAMS = (
    ("blue", "Blue"),
    ("gold", "Gold"),
)

PLAYERS_PER_MATCH = 10


def timedelta_format(td):
    return "{}:{:02}".format(math.floor(td.total_seconds() / 60), td.seconds % 60)


class Cabinet(models.Model):
    name = models.CharField(max_length=20, unique=True)
    display_name = models.CharField(max_length=200)
    token = models.CharField(max_length=200)


class Player(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def results_by_season(self):
        seasons = {}

        for team in self.team_set.all():
            event = team.event
            season = event.season
            if season.id not in seasons:
                seasons[season.id] = {"season": season, "events": {}}

            event_result = seasons[season.id]["events"][event.id] = team.results()
            event_result["event"] = event

        for qp_player in self.qpplayer_set.distinct("qp_match__event_id"):
            event = qp_player.qp_match.event
            season = event.season
            if season.id not in seasons:
                seasons[season.id] = {"season": season, "events": {}}

            event_results = event.qp_player_results()
            for player_result in event_results:
                if player_result["player"].id == self.id:
                    event_result = seasons[season.id]["events"][event.id] = player_result
                    event_result["event"] = event

        season_results_sorted = []
        for season in sorted(seasons.values(), key=lambda x: x["season"].start_date):
            season_result = {"season": season["season"], "events": [], "rounds_won": 0, "rounds_lost": 0, "matches_won": 0, "matches_lost": 0, "points": 0}

            season_result["events"] = sorted(season["events"].values(), key=lambda x: x["event"].event_date)

            max_events_counted = season["season"].event_set.count() - season["season"].drop_lowest_scores
            for event_result in sorted(season_result["events"], key=lambda x: x["points"], reverse=True)[:max_events_counted]:
                for k in ["rounds_won", "rounds_lost", "matches_won", "matches_lost", "points"]:
                    season_result[k] += event_result[k]

            season_results_sorted.append(season_result)

        return season_results_sorted

    @classmethod
    def by_name(cls, name):
        try:
            return cls.objects.filter(name=name).get()
        except cls.DoesNotExist:
            return cls(name=name)

    def rating(self):
        rating = PlayerRating.objects.filter(player=self).order_by("-rating_date").first()
        if rating is None:
            return trueskill_env.create_rating()
        else:
            return rating.rating()


class Season(models.Model):
    name = models.CharField(max_length=200, unique=True)
    start_date = models.DateTimeField(null=True)
    end_date = models.DateTimeField(null=True)
    drop_lowest_scores = models.IntegerField(default=0)
    match_count_base = models.IntegerField(default=0)
    is_byot = models.BooleanField(default=False)

    def team_results(self):
        results = [i.results() for i in self.byoteam_set.all()]
        return sorted(results, reverse=True, key=lambda i: i["points"])

    def player_results(self):
        results_by_player = {}

        for event in self.event_set.all():
            if event.is_quickplay:
                player_results = event.qp_player_results()
                for result in player_results:
                    if result["player"].id not in results_by_player:
                        results_by_player[result["player"].id] = {"player": result["player"], "events": []}

                    results_by_player[result["player"].id]["events"].append(result)

            else:
                team_results = event.team_results()

                for team_result in team_results:
                    for player in team_result["team"].players.all():
                        if player.id not in results_by_player:
                            results_by_player[player.id] = {"player": player, "events": []}

                        results_by_player[player.id]["events"].append(team_result)

        results = []
        max_events_counted = self.event_set.count() - self.drop_lowest_scores
        for player_id, player_results in results_by_player.items():
            result = {"points": 0, "rounds_won": 0, "rounds_lost": 0, "matches_won": 0, "matches_lost": 0, "player": player_results["player"]}
            player_results["events"].sort(key=lambda x: x["points"], reverse=True)
            for event_result in player_results["events"][:max_events_counted]:
                for k in ["rounds_won", "rounds_lost", "matches_won", "matches_lost", "points"]:
                    result[k] += event_result[k]

            results.append(result)

        return sorted(results, key=lambda x: x["points"], reverse=True)


class Event(models.Model):
    event_date = models.DateTimeField()
    season = models.ForeignKey(Season, on_delete=models.CASCADE)
    cabinet = models.ForeignKey(Cabinet, null=True, on_delete=models.SET_NULL)
    is_complete = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    is_quickplay = models.BooleanField(default=False)
    unassigned_players = models.ManyToManyField(Player)
    points_per_round = models.IntegerField(default=0)
    points_per_match = models.IntegerField(default=0)
    rounds_per_match = models.IntegerField(default=0)
    qp_matches_per_player = models.IntegerField(default=0)

    def team_results(self):
        results = {}

        for team in self.team_set.all():
            results[team.id] = team.results()

        return sorted(results.values(), key=lambda x: (x["points"], -x["team"].id), reverse=True)

    def qp_player_results(self):
        results = {}

        for qp_match in self.qpmatch_set.all():
            match_result = {
                "blue": {
                    "rounds_won": qp_match.blue_score,
                    "rounds_lost": qp_match.gold_score,
                    "matches_won": (1 if qp_match.blue_score > qp_match.gold_score else 0),
                    "matches_lost": (1 if qp_match.gold_score > qp_match.blue_score else 0),
                },
                "gold": {
                    "rounds_won": qp_match.gold_score,
                    "rounds_lost": qp_match.blue_score,
                    "matches_won": (1 if qp_match.gold_score > qp_match.blue_score else 0),
                    "matches_lost": (1 if qp_match.blue_score > qp_match.gold_score else 0),
                },
            }

            for qp_player in qp_match.qpplayer_set.all():
                player_result = results[qp_player.player.id] = results.get(qp_player.player.id, {})
                player_result["player"] = qp_player.player

                for k in ("rounds_won", "rounds_lost", "matches_won", "matches_lost"):
                    player_result[k] = player_result.get(k, 0) + match_result[qp_player.team][k]

                player_result["points"] = player_result["rounds_won"] * self.points_per_round + \
                    player_result["matches_won"] * self.points_per_match

        return sorted(results.values(), key=lambda x: (x["points"], x["rounds_won"]), reverse=True)

    def event_number(self):
        return self.season.event_set.filter(event_date__lte=self.event_date).count()

    def generate_teams(self, num_teams):
        if self.season.is_byot:
            for team in self.season.byoteam_set.all():
                team.create_team_for_event(self)

            self.generate_matches()
            return

        teams = [Team(event=self, name="Team #{}".format(i+1)) for i in range(num_teams)]
        Team.objects.bulk_create(teams)

        if self.unassigned_players.count() > 0:
            players = self.unassigned_players.all()

            player_points = {i["player"].id: i["points"] for i in self.season.player_results()}
            player_ratings = {i.id: i.rating().mu for i in players}

            season_pct_complete = self.event_number() / self.season.event_set.count()

            if player_points:
                points_weight = season_pct_complete * (max(player_points.values()) - min(player_points.values()))
                rating_weight = (1 - season_pct_complete) * (max(player_ratings.values()) - min(player_ratings.values()))
            else:
                points_weight = 0
                rating_weight = 1

            rating_sort = lambda i: (player_points.get(i.id, 0) * points_weight + player_ratings.get(i.id, 0) * rating_weight)

            ranked_players = sorted(players, key=rating_sort, reverse=True)

            for player, team in zip(ranked_players, itertools.cycle(teams + teams[::-1])):
                team.players.add(player)

            self.unassigned_players.clear()

        self.generate_matches()

    def generate_matches(self):
        schedules_by_team_count = {
            3: [
                (0, 1),
                (0, 2),
                (1, 2),
                (1, 0),
                (2, 0),
                (2, 1),
            ],
            4: [
                (0, 1),
                (2, 3),
                (0, 2),
                (1, 3),
                (1, 2),
                (0, 3),
            ],
            5: [
                (0, 1),
                (2, 3),
                (0, 4),
                (1, 2),
                (3, 4),
                (0, 2),
                (1, 3),
                (2, 4),
                (0, 3),
                (1, 4),
            ],
            6: [
                (0, 1),
                (2, 3),
                (4, 5),
                (0, 2),
                (1, 3),
                (2, 4),
                (3, 5),
                (1, 4),
                (0, 3),
                (2, 5),
                (0, 4),
                (1, 5),
                (3, 4),
                (0, 5),
                (1, 2),
            ],
        }

        teams = self.team_set.order_by("id").all()

        if len(teams) in schedules_by_team_count:
            schedule = schedules_by_team_count[len(teams)]
        else:
            schedule = [sorted(i, key=lambda x: random.random()) for i in itertools.combinations(range(len(teams)))]
            random.shuffle(schedule)

        for matchup in schedule:
            Match(event=self, blue_team=teams[matchup[0]], gold_team=teams[matchup[1]]).save()

    def generate_qp_match(self):
        total_player_rounds = self.unassigned_players.count() * int(self.qp_matches_per_player)
        total_remaining_player_rounds = total_player_rounds
        for qpmatch in self.qpmatch_set.all():
            total_remaining_player_rounds -= qpmatch.qpplayer_set.count()

        if total_remaining_player_rounds <= 0:
            self.is_complete = True
            self.is_active = False
            self.unassigned_players.clear()
            self.save()

            return

        previous_teams = set()
        for qpmatch in self.qpmatch_set.all():
            previous_teams.add(frozenset([i.player_id for i in qpmatch.qpplayer_set.filter(team="blue")]))
            previous_teams.add(frozenset([i.player_id for i in qpmatch.qpplayer_set.filter(team="gold")]))

        if total_remaining_player_rounds % PLAYERS_PER_MATCH > 0:
            player_count = PLAYERS_PER_MATCH - 2
        else:
            player_count = PLAYERS_PER_MATCH

        matches_by_player = {i.id: 0 for i in self.unassigned_players.all()}
        for qp_match in self.qpmatch_set.all():
            for qp_player in qp_match.qpplayer_set.all():
                matches_by_player[qp_player.player.id] += 1

        players = []
        while len(players) < player_count:
            added_players = [k for k, v in matches_by_player.items() if v == min(matches_by_player.values())]
            if len(players) + len(added_players) > player_count:
                added_players = random.sample(added_players, player_count - len(players))

            players.extend(added_players)
            for player in added_players:
                del matches_by_player[player]

        player_ratings = {i: Player.objects.get(id=i).rating() for i in players}
        picked_team = None
        best_quality = 0

        for blue_players in itertools.combinations(players, int(player_count / 2)):
            gold_players = [i for i in players if i not in blue_players]

            if frozenset(blue_players) in previous_teams:
                continue
            if frozenset(gold_players) in previous_teams:
                continue

            blue_ratings = [player_ratings[i] for i in blue_players]
            gold_ratings = [player_ratings[i] for i in gold_players]

            match_quality = trueskill_env.quality((blue_ratings, gold_ratings))
            if match_quality > best_quality:
                picked_team = blue_players
                best_quality = match_quality

        qp_match = QPMatch()
        qp_match.event = self
        qp_match.save()

        for player_id in players:
            qp_player = QPPlayer()
            qp_player.player_id = player_id
            qp_player.qp_match = qp_match
            qp_player.team = "blue" if player_id in picked_team else "gold"
            qp_player.save()

        return qp_match

    def current_match(self):
        return self.match_set.filter(is_complete=False).order_by("id").first()

    def current_qp_match(self):
        return self.qpmatch_set.filter(is_complete=False).order_by("id").first()


class BYOTeam(models.Model):
    season = models.ForeignKey(Season, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    players = models.ManyToManyField(Player)

    def results(self):
        totals = {"team": self, "rounds_won": 0, "rounds_lost": 0, "matches_won": 0, "matches_lost": 0, "points": 0}
        for team in self.team_set.all():
            week_results = team.results()
            for k in ["rounds_won", "rounds_lost", "matches_won", "matches_lost", "points"]:
                totals[k] += week_results[k]

        return totals

    def create_team_for_event(self, event):
        team = Team(event=event, name=self.name, byoteam=self)
        team.save()

        for player in self.players.all():
            team.players.add(player)

        return team


class Team(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    players = models.ManyToManyField(Player)
    byoteam = models.ForeignKey(BYOTeam, null=True, on_delete=models.SET_NULL)

    def results(self):
        results = {"team": self, "rounds_won": 0, "rounds_lost": 0, "matches_won": 0, "matches_lost": 0}
        for match in self.matches():
            scores = match.score_by_team_id()
            results["rounds_won"] += scores[self.id]
            results["rounds_lost"] += sum([v for k, v in scores.items() if k != self.id])
            if match.is_complete:
                if scores[self.id] == max(scores.values()):
                    results["matches_won"] += 1
                else:
                    results["matches_lost"] += 1

        results["points"] = self.event.points_per_round * results["rounds_won"] + \
                            self.event.points_per_match * results["matches_won"]

        match_count_base = self.event.season.match_count_base
        if match_count_base and len(self.matches()) > 0:
            results["points"] *= (match_count_base) / (len(self.matches()))

        return results

    def matches(self):
        return Match.objects.filter(blue_team_id=self.id) | Match.objects.filter(gold_team_id=self.id)


class Match(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    blue_team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name="+")
    gold_team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name="+")
    blue_score = models.IntegerField(default=0)
    gold_score = models.IntegerField(default=0)
    is_complete = models.BooleanField(default=False)

    def score_by_team_id(self):
        return {
            self.blue_team_id: self.blue_score,
            self.gold_team_id: self.gold_score,
        }

    def rate_players(self):
        blue_players = self.blue_team.players.all()
        gold_players = self.gold_team.players.all()
        blue_ratings = {player: player.rating() for player in blue_players}
        gold_ratings = {player: player.rating() for player in gold_players}

        ranks = [0 if self.blue_score > self.gold_score else 1, 0 if self.gold_score > self.blue_score else 1]
        rating_groups = trueskill_env.rate([blue_ratings, gold_ratings], ranks=ranks)
        for group in rating_groups:
            for player, rating in group.items():
                PlayerRating(player=player, match=self, mu=rating.mu, sigma=rating.sigma).save()


class QPMatch(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    blue_score = models.IntegerField(default=0)
    gold_score = models.IntegerField(default=0)
    is_complete = models.BooleanField(default=False)

    def rate_players(self):
        blue_players = [i.player for i in self.qpplayer_set.filter(team="blue")]
        gold_players = [i.player for i in self.qpplayer_set.filter(team="gold")]
        blue_ratings = {player: player.rating() for player in blue_players}
        gold_ratings = {player: player.rating() for player in gold_players}

        ranks = [0 if self.blue_score > self.gold_score else 1, 0 if self.gold_score > self.blue_score else 1]
        rating_groups = trueskill_env.rate([blue_ratings, gold_ratings], ranks=ranks)
        for group in rating_groups:
            for player, rating in group.items():
                PlayerRating(player=player, qp_match=self, mu=rating.mu, sigma=rating.sigma).save()


class QPPlayer(models.Model):
    qp_match = models.ForeignKey(QPMatch, on_delete=models.CASCADE)
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    team = models.CharField(max_length=5, null=True, choices=TEAMS)


class Tournament(models.Model):
    name = models.CharField(max_length=200)
    is_active = models.BooleanField(default=False)

    def get_active_matches(self):
        bracket_ids = [i.id for i in self.tournamentbracket_set.all()]
        matches = TournamentMatch.objects.filter(active_cabinet__isnull=False, bracket_id__in=bracket_ids)

        return matches

    def get_active_match_by_cabinet(self, cabinet_id):
        bracket_ids = [i.id for i in self.tournamentbracket_set.all()]
        try:
            return TournamentMatch.objects.get(active_cabinet_id=cabinet_id, bracket_id__in=bracket_ids)
        except TournamentMatch.DoesNotExist:
            return None

    def get_available_matches(self):
        bracket_ids = [i.id for i in self.tournamentbracket_set.order_by("id").all()]
        matches = TournamentMatch.objects.filter(active_cabinet__isnull=True, is_complete=False, bracket_id__in=bracket_ids) \
            .order_by("id")

        return matches


class TournamentBracket(models.Model):
    name = models.CharField(max_length=200)
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    challonge_id = models.CharField(max_length=200, unique=True)
    rounds_per_match = models.IntegerField(null=True)
    wins_per_match = models.IntegerField(null=True)

    def get_participants(self):
        url = "https://api.challonge.com/v1/tournaments/{}/participants.json".format(self.challonge_id)
        request = requests.get(url, params={"api_key": settings.CHALLONGE_API_KEY})

        participants = {}
        for row in request.json():
            participants[row["participant"]["id"]] = row["participant"]["name"]

        return participants

    def get_matches(self):
        url = "https://api.challonge.com/v1/tournaments/{}/matches.json".format(self.challonge_id)
        request = requests.get(url, params={"api_key": settings.CHALLONGE_API_KEY})

        participants = self.get_participants()

        for row in request.json():
            match = row["match"]
            if match["state"] == "open":
                try:
                    tm = TournamentMatch.objects.get(challonge_id=match["id"])
                except TournamentMatch.DoesNotExist:
                    tm = TournamentMatch()

                tm.bracket = self
                tm.challonge_id = match["id"]
                tm.blue_team = participants[match["player1_id"]]
                tm.gold_team = participants[match["player2_id"]]
                tm.save()

    def get_standings(self):
        url = "https://api.challonge.com/v1/tournaments/{}.json".format(self.challonge_id)
        request = requests.get(url, params={
            "api_key": settings.CHALLONGE_API_KEY,
            "include_participants": 1,
            "include_matches": 1,
        })

        request.raise_for_status()

        response = request.json()["tournament"]
        if response["tournament_type"] != "swiss":
            return None

        participants = {}
        for row in response["participants"]:
            participants[row["participant"]["id"]] = row["participant"]["name"]

        standings = {k: {"name": v, "score": 0} for k, v in participants.items()}
        rounds = {}

        for row in response["matches"]:
            match = row["match"]
            blue_id = match["player1_id"]
            gold_id = match["player2_id"]

            if match["state"] != "pending":
                if match["round"] not in rounds:
                    rounds[match["round"]] = set()

                rounds[match["round"]].add(blue_id)
                rounds[match["round"]].add(gold_id)

            if match["state"] == "complete":
                for score in match["scores_csv"].split(","):
                    blue_score, gold_score = score.split("-")

                    if int(blue_score) > int(gold_score):
                        standings[blue_id]["score"] += float(response["pts_for_game_win"])
                    if int(gold_score) > int(blue_score):
                        standings[gold_id]["score"] += float(response["pts_for_game_win"])
                    if int(blue_score) == int(gold_score):
                        standings[blue_id]["score"] += float(response["pts_for_game_tie"])
                        standings[gold_id]["score"] += float(response["pts_for_game_tie"])

                standings[match["winner_id"]]["score"] += float(response["pts_for_match_win"])

        for team_set in rounds.values():
            for team_id, team_score in standings.items():
                if team_id not in team_set:
                    team_score["score"] += float(response["pts_for_bye"])

        return sorted(standings.values(), reverse=True, key=lambda i: i["score"])


class TournamentMatch(models.Model):
    bracket = models.ForeignKey(TournamentBracket, on_delete=models.CASCADE)
    challonge_id = models.CharField(max_length=200, unique=True)
    blue_team = models.CharField(max_length=200)
    gold_team = models.CharField(max_length=200)
    blue_score = models.IntegerField(default=0)
    gold_score = models.IntegerField(default=0)
    active_cabinet = models.ForeignKey(Cabinet, null=True, on_delete=models.SET_NULL)
    is_complete = models.BooleanField(default=False)

    def report_result(self):
        url = "https://api.challonge.com/v1/tournaments/{}/matches/{}.json".format(self.bracket.challonge_id, self.challonge_id)
        request = requests.get(url, params={"api_key": settings.CHALLONGE_API_KEY})

        blue_team_id = request.json()["match"]["player1_id"]
        gold_team_id = request.json()["match"]["player2_id"]

        scores = []
        for i in range(self.blue_score):
            scores.append("1-0")
        for i in range(self.gold_score):
            scores.append("0-1")
        scores_csv = ",".join(scores)

        if self.blue_score > self.gold_score:
            winning_team = blue_team_id
        if self.gold_score > self.blue_score:
            winning_team = gold_team_id

        request = requests.put(url, json={
            "api_key": settings.CHALLONGE_API_KEY,
            "match": {
                "scores_csv": scores_csv,
                "winner_id": winning_team,
            },
        })


class Game(models.Model):
    WIN_CONDITIONS = (
        ("military", "Military"),
        ("economic", "Economic"),
        ("snail", "Snail"),
    )

    MAP_NAMES = {
        "map_bonus_military": "Bonus (Military)",
        "map_bonus_snail": "Bonus (Snail)",
        "map_day": "Day",
        "map_dusk": "Dusk",
        "map_night": "Night",
        "map_twilight2": "Twilight",
    }

    PLAYER_IMAGES = {
        1: "/static/players/gold-queen.png",
        2: "/static/players/blue-queen.png",
        3: "/static/players/gold-drone-stripes.png",
        4: "/static/players/blue-drone-stripes.png",
        5: "/static/players/gold-drone-abs.png",
        6: "/static/players/blue-drone-abs.png",
        7: "/static/players/gold-drone-skulls.png",
        8: "/static/players/blue-drone-skulls.png",
        9: "/static/players/gold-drone-checks.png",
        10: "/static/players/blue-drone-checks.png",
    }

    match = models.ForeignKey(Match, null=True, on_delete=models.SET_NULL)
    qp_match = models.ForeignKey(QPMatch, null=True, on_delete=models.SET_NULL)
    tournament_match = models.ForeignKey(TournamentMatch, null=True, on_delete=models.SET_NULL)
    cabinet = models.ForeignKey(Cabinet, null=True, on_delete=models.SET_NULL)
    start_time = models.DateTimeField(null=True)
    end_time = models.DateTimeField(null=True)
    win_condition = models.CharField(max_length=10, null=True, choices=WIN_CONDITIONS)
    winning_team = models.CharField(max_length=5, null=True, choices=TEAMS)
    map_name = models.CharField(max_length=50)

    @classmethod
    def get_game_data(cls, game_id, match_id=None):
        url = "https://www.bmorekq.com/api/matches/{}".format(game_id)
        response = requests.get(url).json()

        obj = cls()
        obj.start_time = response.get("startTime")
        obj.end_time = response.get("endTime")
        obj.win_condition = response.get("winCondition")
        obj.winning_team = response.get("winningTeam")
        obj.map_name = response.get("map")

        if match_id is not None:
            obj.match = Match.objects.get(id=match_id)

        return obj

    def get_winning_team(self):
        if self.match is None:
            return None

        if self.winning_team == "blue":
            return self.match.blue_team
        if self.winning_team == "gold":
            return self.match.gold_team

    def get_map_name_display(self):
        return self.MAP_NAMES.get(self.map_name, "Unknown Map")

    def get_start_time_local(self):
        return self.start_time.astimezone(pytz.timezone("America/New_York"))

    def get_event_count_by_player(self, event_type, value_idx, event_filter=None):
        counts = {}
        for event in self.gameevent_set.filter(event_type=event_type, timestamp__gt=self.start_time):
            if event_filter is not None and not event_filter(event):
                continue

            player_id = int(event.values[value_idx])
            counts[player_id] = counts.get(player_id, 0) + 1

        return counts

    def get_most_events(self, event_type, value_idx, event_filter=None):
        counts = self.get_event_count_by_player(event_type, value_idx, event_filter)
        if not counts:
            return {}

        return {
            "count": max(counts.values()),
            "players": list(filter(lambda i: counts[i] == max(counts.values()), counts.keys())),
        }

    def get_team_event_count(self, event_type, value_idx, event_filter=None):
        count = {"blue": 0, "gold": 0}
        for event in self.gameevent_set.filter(event_type=event_type, timestamp__gt=self.start_time):
            if event_filter is not None and not event_filter(event):
                continue

            player_id = int(event.values[value_idx])
            if player_id % 2:
                count["gold"] += 1
            else:
                count["blue"] += 1

        return count

    def get_game_length_str(self):
        if self.start_time and self.end_time:
            return timedelta_format(self.end_time - self.start_time)

        return ""

    def get_postgame_stats(self):
        data = {}

        if not self.winning_team:
            return data

        data["game_id"] = self.id
        data["start_time"] = self.start_time
        data["end_time"] = self.end_time
        data["winning_team"] = self.winning_team
        data["win_condition"] = self.win_condition
        data["win_description"] = "{} Victory - {}".format(self.get_winning_team_display(), self.get_win_condition_display())
        data["map"] = self.get_map_name_display()

        game_length = self.end_time - self.start_time
        data["length"] = timedelta_format(game_length)
        data["length_sec"] = game_length.total_seconds()

        data["kills"] = self.get_team_event_count("playerKill", 2)
        data["military_kills"] = self.get_team_event_count("playerKill", 2, lambda event: event.values[4] != "Worker")
        data["berries"] = self.get_team_event_count("berryDeposit", 2)

        for event in self.gameevent_set.filter(event_type="berryKickIn"):
            if len(event.values) < 4:
                continue

            own_team = True if event.values[3] == "True" else False
            player_id = int(event.values[2])
            if (player_id % 2) ^ own_team:
                data["berries"]["blue"] += 1
            else:
                data["berries"]["gold"] += 1

        data["most_kills"] = self.get_most_events("playerKill", 2)
        data["most_military_kills"] = self.get_most_events("playerKill", 2, lambda event: event.values[4] != "Worker")
        data["most_deaths"] = self.get_most_events("playerKill", 3)
        data["most_berries"] = self.get_most_events("berryDeposit", 2)

        warrior_uptime = {}
        warrior_up_events = self.gameevent_set.filter(event_type="useMaiden", values__2="maiden_wings", timestamp__gt=self.start_time).all()

        for up_event in warrior_up_events:
            player_id = int(up_event.values[3])
            down_event = self.gameevent_set.filter(event_type="playerKill", values__3=player_id, timestamp__gt=up_event.timestamp) \
                                           .order_by("timestamp").first()

            if down_event is None:
                down_time = self.end_time
            else:
                down_time = down_event.timestamp

            warrior_uptime[player_id] = warrior_uptime.get(player_id, timedelta(0)) + (down_time - up_event.timestamp)

        if warrior_uptime:
            data["most_warrior_uptime"] = {
                "count": timedelta_format(max(warrior_uptime.values())),
                "players": list(filter(lambda i: warrior_uptime[i] == max(warrior_uptime.values()), warrior_uptime.keys())),
            }

        warrior_gates = set()
        for event in self.gameevent_set.filter(event_type="useMaiden", values__2="maiden_wings", timestamp__gt=self.start_time):
            warrior_gates.add(tuple(event.values[0:2]))

        gate_control = {"blue": timedelta(0), "gold": timedelta(0)}
        for gate in warrior_gates:
            last_time = self.start_time
            controlled_by = None
            for event in self.gameevent_set.filter(event_type="blessMaiden", timestamp__gt=self.start_time, values__0=gate[0], values__1=gate[1]).order_by("timestamp"):
                if controlled_by:
                    gate_control[controlled_by] += event.timestamp - last_time

                controlled_by = "blue" if event.values[2] == "Blue" else "gold"
                last_time = event.timestamp

            if controlled_by:
                gate_control[controlled_by] += self.end_time - last_time

        if len(warrior_gates) > 0:
            data["gate_control"] = {k: "{:.01f}%".format(100 * v / (self.end_time - self.start_time) / len(warrior_gates)) for k, v in gate_control.items()}

        snail_data = [{"x": 0, "y": 0}]
        initial_position = None
        for snail_event in self.gameevent_set.filter(timestamp__gt=self.start_time, event_type__in=["getOnSnail", "getOffSnail"]).order_by("timestamp"):
            if initial_position is None:
                initial_position = int(snail_event.values[0])

            snail_data.append({"x": (snail_event.timestamp - self.start_time).total_seconds(), "y": initial_position - int(snail_event.values[0])})

        if self.win_condition == "snail":
            snail_data.append({"x": (self.end_time - self.start_time).total_seconds(), "y": (-900 if self.winning_team == "gold" else 900)})
        else:
            snail_data.append({"x": (self.end_time - self.start_time).total_seconds(), "y": snail_data[-1]["y"]})

        data["snail_data"] = snail_data

        berry_data = {"blue": [{"x": 0, "y": 0}], "gold": [{"x": 0, "y": 0}]}
        for berry_event in self.gameevent_set.filter(timestamp__gt=self.start_time, event_type__in=["berryDeposit", "berryKickIn"]).order_by("timestamp"):
            if berry_event.event_type == "berryKickIn" and len(berry_event.values) >= 4:
                own_team = True if berry_event.values[3] == "True" else False
            else:
                own_team = True

            if (int(berry_event.values[2]) % 2) ^ own_team:
                berry_chart = berry_data["blue"]
            else:
                berry_chart = berry_data["gold"]

            previous_value = berry_chart[-1]["y"]
            berry_chart.append({"x": (berry_event.timestamp - self.start_time).total_seconds(), "y": previous_value})
            berry_chart.append({"x": (berry_event.timestamp - self.start_time).total_seconds(), "y": previous_value + 1})

        berry_data["blue"].append({"x": (self.end_time - self.start_time).total_seconds(), "y": berry_data["blue"][-1]["y"]})
        berry_data["gold"].append({"x": (self.end_time - self.start_time).total_seconds(), "y": berry_data["gold"][-1]["y"]})
        data["berry_data"] = berry_data

        warrior_data = {"blue": [{"x": 0, "y": 0}], "gold": [{"x": 0, "y": 0}]}
        for warrior_event in self.gameevent_set.filter(timestamp__gt=self.start_time, event_type__in=["useMaiden", "playerKill"]).order_by("timestamp"):
            if warrior_event.event_type == "useMaiden" and warrior_event.values[2] != "maiden_wings":
                continue
            if warrior_event.event_type == "playerKill" and warrior_event.values[4] != "Soldier":
                continue

            if int(warrior_event.values[3]) % 2:
                warrior_chart = warrior_data["gold"]
            else:
                warrior_chart = warrior_data["blue"]

            previous_value = warrior_chart[-1]["y"]
            if warrior_event.event_type == "useMaiden":
                new_value = previous_value + 1
            else:
                new_value = previous_value - 1

            warrior_chart.append({"x": (warrior_event.timestamp - self.start_time).total_seconds(), "y": previous_value})
            warrior_chart.append({"x": (warrior_event.timestamp - self.start_time).total_seconds(), "y": new_value})

        warrior_data["blue"].append({"x": (self.end_time - self.start_time).total_seconds(), "y": warrior_data["blue"][-1]["y"]})
        warrior_data["gold"].append({"x": (self.end_time - self.start_time).total_seconds(), "y": warrior_data["gold"][-1]["y"]})
        data["warrior_data"] = warrior_data

        return data


class GameEvent(models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    timestamp = models.DateTimeField()
    event_type = models.CharField(max_length=50)
    values = ArrayField(models.CharField(max_length=50))


class PlayerRating(models.Model):
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    match = models.ForeignKey(Match, null=True, on_delete=models.CASCADE)
    qp_match = models.ForeignKey(QPMatch, null=True, on_delete=models.CASCADE)
    rating_date = models.DateTimeField(default=datetime.now)
    mu = models.FloatField(default=0)
    sigma = models.FloatField(default=0)

    def rating(self):
        return trueskill_env.create_rating(mu=self.mu, sigma=self.sigma)

    @classmethod
    @transaction.atomic
    def recalculate_all(cls):
        cls.objects.all().delete()

        for event in Event.objects.order_by("event_date").all():
            for match in event.match_set.filter(is_complete=True).order_by("id").all():
                match.rate_players()
            for qp_match in event.qpmatch_set.filter(is_complete=True).order_by("id").all():
                qp_match.rate_players()

    def get_event(self):
        if self.match is not None:
            return self.match.event
        if self.qp_match is not None:
            return self.qp_match.event

    def get_team(self):
        if self.match is not None:
            if self.match.blue_team.players.filter(id=self.player.id).count() > 0:
                return "blue"
            if self.match.gold_team.players.filter(id=self.player.id).count() > 0:
                return "gold"

        if self.qp_match is not None:
            return self.qp_match.qpplayer_set.filter(player_id=self.player.id).first().team


class AdminUser(models.Model):
    account_id = models.CharField(max_length=200, unique=True)
    email = models.CharField(max_length=200)
    authorized = models.BooleanField(default=False)

    @classmethod
    def by_account_id(cls, account_id):
        try:
            return cls.objects.filter(account_id=account_id).get()
        except cls.DoesNotExist:
            return cls(account_id=account_id)

    def generate_token(self):
        token = AdminLoginToken(admin_user=self, token=hashlib.sha512(os.urandom(2048)).hexdigest())
        token.save()

        return token


class AdminLoginToken(models.Model):
    admin_user = models.ForeignKey(AdminUser, on_delete=models.CASCADE)
    issue_date = models.DateTimeField(default=datetime.now)
    token = models.CharField(max_length=128, unique=True)
