from datetime import datetime
from django.http import JsonResponse

from kqb.models import Event, Season, Player, Match, Team, Game, Cabinet, Tournament

def now(request):
    data = {}

    active_tournament = Tournament.objects.filter(is_active=True).first()
    if active_tournament:
        data = {
            "tournament_name": active_tournament.name,
            "active_matches": [],
        }

        for match in active_tournament.get_active_matches():
            data["active_matches"].append({
                "blue_team": match.blue_team,
                "blue_score": match.blue_score,
                "gold_team": match.gold_team,
                "gold_score": match.gold_score,
                "cabinet_name": match.active_cabinet.name,
                "cabinet_id": match.active_cabinet.id,
                "bracket_name": match.bracket.name,
            })

        return JsonResponse(data)

    active_event = Event.objects.filter(is_active=True).order_by("event_date").first()

    if not active_event:
        season = Season.objects.order_by("-start_date").first()
        next_event = season.event_set.order_by("event_date").filter(is_complete=False).first()

        if next_event and next_event.event_date:
            data["next_event"] = next_event.event_date.strftime("%Y-%m-%d")

        return JsonResponse(data)

    if active_event.is_quickplay:
        current_match = active_event.current_qp_match()
        if current_match:
            data["current_match"] = {
                "blue_team": "Blue Team",
                "blue_score": current_match.blue_score,
                "gold_team": "Gold Team",
                "gold_score": current_match.gold_score,
            }

    else:
        current_match = active_event.current_match()
        data["current_match"] = {
            "blue_team": current_match.blue_team.name,
            "blue_score": current_match.blue_score,
            "gold_team": current_match.gold_team.name,
            "gold_score": current_match.gold_score,
            "rounds_to_win": active_event.rounds_per_match,
        }

        data["standings"] = [
            {
                "team_id": result["team"].id,
                "team_name": result["team"].name,
                "points": "{:0.2f}".format(result["points"]),
                "rounds": result["rounds_won"],
                "matches": result["matches_won"],
            }
            for result in active_event.team_results()
        ]

        data["matches"] = [
            {
                "is_complete": match.is_complete,
                "blue_team": match.blue_team.name,
                "blue_score": match.blue_score,
                "gold_team": match.gold_team.name,
                "gold_score": match.gold_score,
            }
            for match in active_event.match_set.order_by("id").all()
        ]

    return JsonResponse(data)

def game_by_id(request, game_id):
    data = {}

    game = Game.objects.get(id=game_id)
    if game is None:
        return JsonResponse({})

    return JsonResponse(game.get_postgame_stats())

def last_game(request, cabinet_name):
    cabinet = Cabinet.objects.filter(name__iexact=cabinet_name).first()
    game = Game.objects.filter(cabinet_id=cabinet.id) \
        .exclude(end_time__isnull=True).order_by("-end_time").first()

    return game_by_id(request, game.id)

def tournament_standings(request, tournament_id):
    data = {"brackets": []}
    tournament = Tournament.objects.get(id=tournament_id)

    for bracket in tournament.tournamentbracket_set.order_by("id").all():
        standings = bracket.get_standings()
        if standings:
            data["brackets"].append({
                "name": bracket.name,
                "standings": standings,
            })

    return JsonResponse(data)
