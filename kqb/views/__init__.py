from django.conf import settings
from django.http import HttpResponse, HttpResponseNotAllowed, JsonResponse
from django.template import loader

from kqb.models import Event, Season, Player, Match, QPMatch, Tournament, TournamentMatch, Game


def index(request):
    active_event = Event.objects.filter(is_active=True).order_by("event_date").first()

    if active_event:
        if active_event.is_quickplay:
            template = loader.get_template("event_results_qp.html")
        else:
            template = loader.get_template("event_results.html")

        return HttpResponse(template.render({"event": active_event}, request))

    active_tournament = Tournament.objects.filter(is_active=True).first()

    if active_tournament:
        template = loader.get_template("tournament_results.html")
        return HttpResponse(template.render({"tournament": active_tournament}, request))

    seasons = list(Season.objects.order_by("-start_date").all())
    season = seasons.pop(0)

    tournaments = list(Tournament.objects.order_by("-id").all())

    if season.is_byot:
        template = loader.get_template("byot_season_standings.html")
    else:
        template = loader.get_template("player_standings.html")

    return HttpResponse(template.render({"season": season, "other_seasons": seasons, "tournaments": tournaments}, request))

def player_standings(request, season_id=None):
    if season_id is None:
        season = Season.objects.order_by("-start_date").first()
    else:
        season = Season.objects.get(id=season_id)

    if season.is_byot:
        template = loader.get_template("byot_season_standings.html")
    else:
        template = loader.get_template("player_standings.html")

    return HttpResponse(template.render({"season": season}, request))

def event_results(request, event_id):
    event = Event.objects.get(id=event_id)

    if event.is_quickplay:
        template = loader.get_template("event_results_qp.html")
    else:
        template = loader.get_template("event_results.html")

    return HttpResponse(template.render({"event": event}, request))

def match_results(request, match_id):
    match = Match.objects.get(id=match_id)

    template = loader.get_template("match_results.html")
    return HttpResponse(template.render({"match": match}, request))

def qp_match_results(request, qp_match_id):
    match = QPMatch.objects.get(id=qp_match_id)

    template = loader.get_template("qp_match_results.html")
    return HttpResponse(template.render({"match": match}, request))

def tournament_results(request, tournament_id):
    tournament = Tournament.objects.get(id=tournament_id)

    template = loader.get_template("tournament_results.html")
    return HttpResponse(template.render({"tournament": tournament}, request))

def tournament_match_results(request, match_id):
    match = TournamentMatch.objects.get(id=match_id)

    template = loader.get_template("tournament_match_results.html")
    return HttpResponse(template.render({"match": match}, request))

def game_details(request, game_id):
    game = Game.objects.get(id=game_id)

    template = loader.get_template("game_results.html")
    return HttpResponse(template.render({"game": game, "charts": True}, request))

def player_details(request, player_id):
    player = Player.objects.get(id=player_id)

    template = loader.get_template("player_details.html")
    return HttpResponse(template.render({"player": player}, request))

def obs_overlay(request, cabinet_name=None):
    template = loader.get_template("obs_overlay.html")
    return HttpResponse(template.render({"cabinet_name": cabinet_name}, request))

def obs_postgame(request, cabinet_name):
    template = loader.get_template("obs_postgame.html")
    return HttpResponse(template.render({"cabinet_name": cabinet_name}, request))
