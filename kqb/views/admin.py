import http.client
import json
import os
from django.conf import settings
from django.http import HttpResponse, HttpResponseNotAllowed, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from functools import wraps
from google.oauth2 import id_token
from google.auth.transport import requests

from kqb.models import Cabinet, Event, Season, Player, Match, QPMatch, Team, BYOTeam, \
    PlayerRating, AdminUser, AdminLoginToken, Tournament, TournamentBracket, TournamentMatch

def auth_required(fn):
    @csrf_exempt
    @wraps(fn)
    def wrapped_func(request, *args, **kwargs):
        if "token" in request.COOKIES:
            user = user_from_local_token(request.COOKIES["token"])
            if user.authorized:
                return fn(request, *args, **kwargs)

        if "App-Token" in request.headers and "APP_USER_TOKEN" in os.environ:
            if request.headers["App-Token"] == os.environ["APP_USER_TOKEN"]:
                return fn(request, *args, **kwargs)

        return HttpResponse("Unauthorized", status=http.client.UNAUTHORIZED)

    return wrapped_func

def admin_get(fn):
    @auth_required
    @wraps(fn)
    def wrapped_func(request, *args, **kwargs):
        if request.method != "GET":
            return HttpResponseNotAllowed(["GET"])

        response = fn(request, *args, **kwargs)
        return JsonResponse(response, safe=False)

    return wrapped_func

def admin_post(fn):
    @auth_required
    @wraps(fn)
    def wrapped_func(request, *args, **kwargs):
        if request.method != "POST":
            return HttpResponseNotAllowed(["POST"])

        data = json.loads(request.body)
        response = fn(request, data, *args, **kwargs)
        return JsonResponse(response, safe=False)

    return wrapped_func

def admin_api_post(cls):
    def outer_fn(fn):
        @admin_post
        @wraps(fn)
        def inner_fn(request, data, *args, **kwargs):
            if data.get("id") is None:
                obj = cls()
            else:
                obj = cls.objects.get(id=data["id"])

            for k, v in data.items():
                if k != "id" and not k.startswith("_"):
                    setattr(obj, k, v)

            fn(request, data, obj)
            obj.save()
            return obj_data(obj)

        return inner_fn
    return outer_fn

def obj_data(obj):
    return {k: v for k, v in obj.__dict__.items() if not k.startswith("_")}

def user_from_google_token(token):
    try:
        idinfo = id_token.verify_oauth2_token(token, requests.Request(), settings.OAUTH_CLIENT_ID)
        if idinfo["iss"] not in ["accounts.google.com", "https://accounts.google.com"]:
            return None

        user = AdminUser.by_account_id(idinfo["sub"])
        if user.id is None:
            user.email = idinfo["email"]
            user.save()

        return user

    except ValueError:
        return None

def user_from_local_token(token):
    try:
        token = AdminLoginToken.objects.filter(token=token).get()
        return token.admin_user

    except:
        return None

def render_template(request, template_name, context=None):
    if context is None:
        context = {}

    context["settings"] = settings
    if "token" in request.COOKIES:
        context["adminuser"] = user_from_local_token(request.COOKIES["token"])

    template = loader.get_template(template_name)
    return HttpResponse(template.render(context, request))

def indexpage(request):
    seasons = Season.objects.order_by("start_date").all()
    return render_template(request, "admin_index.html", {"seasons": seasons})

@csrf_exempt
def auth(request):
    data = json.loads(request.body)
    user = user_from_google_token(data["token"])
    if user.authorized:
        return JsonResponse({"token": user.generate_token().token})

    return HttpResponse("Unauthorized", status=http.client.UNAUTHORIZED)

def season(request, season_id=None):
    context = {}
    if season_id:
        context["season"] = Season.objects.get(id=season_id)

    return render_template(request, "admin_season.html", context)

def event(request, event_id):
    event = Event.objects.get(id=event_id)
    cabinets = Cabinet.objects.order_by("id").all()
    return render_template(request, "admin_event.html", {"event": event, "cabinets": cabinets})

def team(request, team_id):
    team = Team.objects.get(id=team_id)
    return render_template(request, "admin_team.html", {"team": team})

def byoteam(request, team_id):
    team = BYOTeam.objects.get(id=team_id)
    return render_template(request, "admin_byoteam.html", {"team": team})

def match(request, match_id=None, event_id=None):
    context = {}
    if match_id is not None:
        context["match"] = Match.objects.get(id=match_id)
        context["event"] = context["match"].event
    if event_id is not None:
        context["event"] = Event.objects.get(id=event_id)

    return render_template(request, "admin_match.html", context)

def players(request):
    players = Player.objects.order_by("name").all()
    season = Season.objects.order_by("-start_date").first()
    results = {i["player"].id: i for i in season.player_results()}
    return render_template(request, "admin_players.html", {"players": players, "season": season, "results": results})

def player_detail(request, player_id):
    player = Player.objects.get(id=player_id)
    ratings = player.playerrating_set.order_by("rating_date").all()
    ratings_zipped = zip(ratings, [None] + list(ratings))
    return render_template(request, "admin_player_detail.html", {"player": player, "ratings": ratings_zipped})

def tournaments(request):
    tournaments = Tournament.objects.order_by("id").all()
    return render_template(request, "admin_tournaments.html", {"tournaments": tournaments})

def tournament_detail(request, tournament_id=None):
    context = {}
    if tournament_id:
        context["tournament"] = Tournament.objects.get(id=tournament_id)
        context["cabinets"] = Cabinet.objects.order_by("id").all()

    return render_template(request, "admin_tournament.html", context)

def users(request):
    users = AdminUser.objects.order_by("email").all()
    return render_template(request, "admin_users.html", {"users": users})

@admin_api_post(Season)
def post_season(request, data, season):
    pass

@admin_api_post(Event)
def post_event(request, data, event):
    if event and event.is_active and event.is_quickplay and not event.is_complete:
        if event.qpmatch_set.count() == 0:
            event.generate_qp_match()

@admin_api_post(Player)
def post_player(request, data, event):
    pass

@admin_api_post(Match)
def post_match(request, data, match):
    if match.is_complete:
        if match.event.match_set.filter(is_complete=False).exclude(id=match.id).count() == 0:
            match.event.is_complete = True
            match.event.save()

        if match.playerrating_set.count() == 0:
            match.rate_players()

@admin_api_post(QPMatch)
def post_qp_match(request, data, qp_match):
    if qp_match.is_complete:
        if qp_match.playerrating_set.count() == 0:
            qp_match.rate_players()

        if qp_match.event.is_active:
            qp_match.event.generate_qp_match()

@admin_api_post(Team)
def post_team(request, data, team):
    pass

@admin_api_post(BYOTeam)
def post_byo_team(request, data, team):
    pass

@admin_api_post(AdminUser)
def post_user(request, data, team):
    pass

@admin_api_post(Tournament)
def post_tournament(request, data, tournament):
    pass

@admin_post
def team_add_player(request, data):
    team = Team.objects.get(id=data["team_id"])
    player = Player.objects.get(id=data["player_id"])

    team.players.add(player)
    return {"team": obj_data(team), "player": obj_data(player)}

@admin_post
def team_remove_player(request, data):
    team = Team.objects.get(id=data["team_id"])
    player = Player.objects.get(id=data["player_id"])

    team.players.remove(player)
    return {"team": obj_data(team), "player": obj_data(player)}

@admin_post
def byoteam_add_player(request, data):
    team = BYOTeam.objects.get(id=data["team_id"])
    player = Player.objects.get(id=data["player_id"])

    team.players.add(player)
    return {"team": obj_data(team), "player": obj_data(player)}

@admin_post
def byoteam_remove_player(request, data):
    team = BYOTeam.objects.get(id=data["team_id"])
    player = Player.objects.get(id=data["player_id"])

    team.players.remove(player)
    return {"team": obj_data(team), "player": obj_data(player)}

@admin_post
def event_add_player(request, data):
    event = Event.objects.get(id=data["event_id"])
    player = Player.objects.get(id=data["player_id"])

    event.unassigned_players.add(player)
    return {"event": obj_data(event), "player": obj_data(player)}

@admin_post
def event_remove_player(request, data):
    event = Event.objects.get(id=data["event_id"])
    player = Player.objects.get(id=data["player_id"])

    event.unassigned_players.remove(player)

    if player.team_set.count() == 0 and player.event_set.count() == 0 and player.playerrating_set.count() == 0:
        player.delete()

    return {"event": obj_data(event), "player": obj_data(player)}

@admin_post
def event_generate_teams(request, data):
    event = Event.objects.get(id=data["event_id"])
    event.generate_teams(data["num_teams"])

    return {"event": obj_data(event), "teams": [obj_data(i) for i in event.team_set.all()]}

@admin_post
def event_delete_teams(request, data):
    event = Event.objects.get(id=data["event_id"])

    event.match_set.all().delete()

    for team in event.team_set.all():
        event.unassigned_players.add(*team.players.all())
        team.delete()

@admin_get
def search_player(request, searchval):
    return [obj_data(i) for i in Player.objects.filter(name__icontains=searchval).order_by("name")]

@admin_post
def rating_recalculate(request, data):
    PlayerRating.recalculate_all()

@admin_post
def generate_qp_match(request, data):
    event = Event.objects.get(id=data["event_id"])

    for qp_match in event.qpmatch_set.all():
        if qp_match.is_complete and qp_match.playerrating_set.count() == 0:
            qp_match.rate_players()

    if event and event.is_active and event.is_quickplay:
        event.generate_qp_match()

@admin_post
def complete_tournament_match(request, data):
    match = TournamentMatch.objects.get(id=data["tournament_match_id"])
    match.active_cabinet = None
    match.is_complete = True
    match.save()

    match.report_result()
    match.bracket.get_matches()

@admin_get
def get_available_tournament_matches(request, tournament_id):
    tournament = Tournament.objects.get(id=tournament_id)
    matches = [{
        "match_id": match.id,
        "bracket_name": match.bracket.name,
        "blue_team": match.blue_team,
        "gold_team": match.gold_team,
    } for match in tournament.get_available_matches()]

    return {"tournament_id": tournament.id, "matches": matches}

@admin_post
def set_tournament_match_cabinet(request, data):
    if data.get("active_cabinet_id"):
        for match in TournamentMatch.objects.filter(active_cabinet_id=data["active_cabinet_id"]):
            match.active_cabinet_id = None
            match.save()

    if data.get("id"):
        match = TournamentMatch.objects.get(id=data["id"])
        match.active_cabinet_id = data.get("active_cabinet_id")
        match.save()
