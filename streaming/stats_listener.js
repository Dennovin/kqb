#!/usr/bin/env node
const http = require("http");
const request = require("request");
const url = require("url");
const WebSocket = require("ws");
const redis = require("redis");
const { Pool, Client } = require("pg");

function log(message) {
    console.log((new Date()) + " " + message);
}

function heartbeat() {
    this.isAlive = true;
}

const server = http.createServer(function(request, response) {
    response.writeHead(404);
    response.end();
});

const messageRegex = new RegExp("(\\d+) \\= \\!\\[\\k\\[(\\w+).*?\\]\,v\\[(.*?)\\]\\]\\!");
function parseMessage(message) {
    var result = messageRegex.exec(message);
    if(result) {
        return {
            timestamp: new Date(parseInt(result[1])),
            messageType: result[2],
            values: result[3].split(",")
        };
    }
}

const pgClient = new Pool({
    user: "postgres",
    host: "db",
    database: "postgres",
    password: process.env.POSTGRES_PASSWORD,
});

pgClient.connect();

const redisClient = redis.createClient({host: process.env.REDIS_HOST});

async function setMatchComplete(id) {
    log(`Match complete`);

    var result = await pgClient.query("update kqb_match set is_complete = true where id = $1 returning event_id", [id]);
    var event = result.rows[0];
    log(`Event ID is ${event.event_id}`);

    result = await pgClient.query("select * from kqb_match where event_id = $1 and is_complete = false", [event.event_id]);
    if(result.rows.length == 0) {
        log(`Event complete`);
        await pgClient.query("update kqb_event set is_active = false, is_complete = true where id = $1", [event.id]);
    }
}

async function setQPMatchComplete(id) {
    log(`Quickplay match complete`);

    var result = await pgClient.query("update kqb_qpmatch set is_complete = true where id = $1 returning event_id", [id]);
    var event = result.rows[0];
    log(`Event ID is ${event.event_id}`);

    var requestURL = url.resolve(process.env.API_BASE_URL, "generate-qp-match");
    request.post({
        url: requestURL,
        json: { event_id: event.event_id },
        headers: { "App-Token": process.env.APP_USER_TOKEN }
    });
}

async function setTournamentMatchComplete(id) {
    log(`Tournament match complete`);

    await pgClient.query("update kqb_tournamentmatch set active_cabinet_id = null where id = $1", [id]);

    var requestURL = url.resolve(process.env.API_BASE_URL, "complete-tournament-match");
    request.post({
        url: requestURL,
        json: { tournament_match_id: id },
        headers: { "App-Token": process.env.APP_USER_TOKEN }
    });
}

async function startGame(message) {
    var result = await pgClient.query("insert into kqb_game(start_time, map_name, cabinet_id) values($1, $2, $3) returning id",
                                      [message.timestamp, message.values[0], message.cabinetID]);
    log(`(${message.cabinetName}) Game ${result.rows[0].id} started (${message.values[0]}).`);
    redisClient.publish("gamestate", JSON.stringify({"type": "gamestart", "game_id": result.rows[0].id}));

    return result.rows[0].id;
}

async function insertMessages(gameID, startTime, messages) {
    await pgClient.query("begin");
    var victoryMessage = null;

    for(var message of messages) {
        if(message.timestamp >= startTime) {
            await pgClient.query("insert into kqb_gameevent(timestamp, event_type, values, game_id) values($1, $2, $3, $4)",
                                 [message.timestamp, message.messageType, message.values, gameID]);

            if(message.messageType == "victory") {
                victoryMessage = message;
            }
        }
    }

    await pgClient.query("commit");

    if(victoryMessage !== null) {
        await updateResult(gameID, victoryMessage);
        return true;
    }

    return false;
}

async function updateResult(gameID, victoryMessage) {
    var mapName = null;
    var gameStart = null;

    log(`(${victoryMessage.cabinetName}) Game complete (${victoryMessage.values[0]} ${victoryMessage.values[1]}).`);

    await pgClient.query("begin");

    var result = await pgClient.query("update kqb_game set end_time = $1, win_condition = $2, winning_team = $3 where id = $4",
                                      [victoryMessage.timestamp, victoryMessage.values[1], victoryMessage.values[0].toLowerCase(), gameID]);

    result = await pgClient.query("select * from kqb_tournamentmatch where active_cabinet_id = $1", [victoryMessage.cabinetID]);
    if(result.rows.length > 0) {
        const match = result.rows[0];
        log(`(${victoryMessage.cabinetName}) Tournament game (match ID is ${match.id})`);

        await pgClient.query("update kqb_game set tournament_match_id = $1 where id = $2", [match.id, gameID]);

        result = await pgClient.query("select * from kqb_tournamentbracket where id = $1", [match.bracket_id]);
        const bracket = result.rows[0];

        if(victoryMessage.values[0].toLowerCase() == "blue") {
            await pgClient.query("update kqb_tournamentmatch set blue_score = blue_score + 1 where id = $1", [match.id]);
            if(bracket.wins_per_match && match.blue_score + 1 >= bracket.wins_per_match) {
                setTimeout(() => { setTournamentMatchComplete(match.id); }, 10000);
            }
        } else {
            await pgClient.query("update kqb_tournamentmatch set gold_score = gold_score + 1 where id = $1", [match.id]);
            if(bracket.wins_per_match && match.gold_score + 1 >= bracket.wins_per_match) {
                setTimeout(() => { setTournamentMatchComplete(match.id); }, 10000);
            }
        }

        if(bracket.rounds_per_match && match.blue_score + match.gold_score + 1 >= bracket.rounds_per_match) {
            setTimeout(() => { setTournamentMatchComplete(match.id); }, 10000);
        }

        await pgClient.query("commit");
        return;
    }

    result = await pgClient.query("select * from kqb_event where is_active = true and cabinet_id = $1", [victoryMessage.cabinetID]);
    if(result.rows.length == 0) {
        await pgClient.query("commit");
        return;
    }

    const event = result.rows[0];

    if(event.is_quickplay) {
        result = await pgClient.query("select * from kqb_qpmatch where event_id = $1 and is_complete = false order by id limit 1", [event.id])
        const match = result.rows[0];

        await pgClient.query("update kqb_game set qp_match_id = $1 where id = $2", [match.id, gameID]);

        if(victoryMessage.values[0].toLowerCase() == "blue") {
            await pgClient.query("update kqb_qpmatch set blue_score = blue_score + 1 where id = $1", [match.id]);
        } else {
            await pgClient.query("update kqb_qpmatch set gold_score = gold_score + 1 where id = $1", [match.id]);
        }

        if(match.blue_score + match.gold_score + 1 >= event.rounds_per_match) {
            setTimeout(() => { setQPMatchComplete(match.id); }, 10000);
        }
    } else {
        result = await pgClient.query("select * from kqb_match where event_id = $1 and is_complete = false order by id limit 1", [event.id])
        const match = result.rows[0];

        await pgClient.query("update kqb_game set match_id = $1 where id = $2", [match.id, gameID]);

        if(victoryMessage.values[0].toLowerCase() == "blue") {
            await pgClient.query("update kqb_match set blue_score = blue_score + 1 where id = $1", [match.id]);

            if(match.blue_score + 1 >= event.rounds_per_match) {
                setTimeout(() => { setMatchComplete(match.id); }, 10000);
            }
        } else {
            await pgClient.query("update kqb_match set gold_score = gold_score + 1 where id = $1", [match.id]);

            if(match.gold_score + 1 >= event.rounds_per_match) {
                setTimeout(() => { setMatchComplete(match.id); }, 10000);
            }
        }
    }

    await pgClient.query("commit");
}

const wss = new WebSocket.Server({ server });

const interval = setInterval(function ping() {
    wss.clients.forEach(function each(ws) {
        if(!ws.isAlive)
            return ws.terminate();

        ws.isAlive = false;
        ws.ping(() => {});
    });
}, 30000);

const queuedMessages = {};
var currentGameID = {};
var currentGameStart = {};
var lastMessagesInsert = null;

wss.on("connection", ws => {
    log("New connection.");
    ws.isAlive = true;
    ws.sentToken = false;
    ws.cabinetID = null;
    ws.cabinetName = null;

    ws.on("pong", heartbeat);

    ws.on("message", messageString => {
        var message = parseMessage(messageString);
        if(!message)
            return;

        message.cabinetID = ws.cabinetID;
        message.cabinetName = ws.cabinetName;

        if(message.messageType == "piOnline") {
            pgClient.query("select * from kqb_cabinet where name = $1", [message.values[1]]).then((result) => {
                if(result.rows.length == 0) {
                    log(`Cabinet "${message.values[1]}" connected, not found in database.`);
                    return;
                }

                const cabinet = result.rows[0];
                if(cabinet.token != message.values[2]) {
                    log(`Invalid token for cabinet "${message.values[1]}".`);
                    return;
                }

                log(`Cabinet "${message.values[1]}" online.`);

                ws.cabinetID = cabinet.id;
                ws.cabinetName = message.values[1];
                ws.sentToken = true;
                currentGameID[ws.cabinetID] = null;
                queuedMessages[ws.cabinetID] = [];
                return;
            });

        } else {
            if(!ws.sentToken)
                return;

            queuedMessages[ws.cabinetID].push(message);
        }
    });

    ws.on("close", () => {
        log("Connection closed.");
    });
});

async function processQueue() {
    for(let cabinetID in queuedMessages) {
        const messageList = queuedMessages[cabinetID];

        for(let message of messageList) {
            if(message.messageType == "gamestart") {
                currentGameID[cabinetID] = await startGame(message);
                currentGameStart[cabinetID] = message.timestamp;
            }
        }

        if(currentGameID[cabinetID] !== null && messageList.length > 0) {
            let insertingMessages = messageList.splice(0, messageList.length);
            let isComplete = await insertMessages(currentGameID[cabinetID], currentGameStart[cabinetID], insertingMessages);

            if(isComplete)
                currentGameID[cabinetID] = null;
        }
    }

    setTimeout(processQueue, 100);
}

setTimeout(processQueue, 100);
server.listen(8080);
