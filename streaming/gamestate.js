#!/usr/bin/env node
const http = require("http");
const request = require("request");
const url = require("url");
const WebSocket = require("ws");
const redis = require("redis");

function log(message) {
    console.log((new Date()) + " " + message);
}

function heartbeat() {
    this.isAlive = true;
}

const redisClient = redis.createClient({host: process.env.REDIS_HOST});
redisClient.subscribe("gamestate");

const server = http.createServer(function(request, response) {
    response.writeHead(404);
    response.end();
});

const wss = new WebSocket.Server({ server });

wss.on("connection", ws => {
    ws.isAlive = true;
    ws.sentToken = false;
    ws.cabinetID = null;
    ws.cabinetName = null;

    ws.on("pong", heartbeat);

    redisClient.on("message", function(channel, message) {
        ws.send(message);
    });
});
